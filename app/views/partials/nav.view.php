<nav>
	<ul class="navbar">
		<li><a href="">Homepage</a></li>
		<li><a href="/about">About us</a></li>
		<li><a href="/contact">Contact us</a></li>
		<?php if( !isset($_SESSION['logged_in']) || $_SESSION['logged_in'] !== true) : ?>
			<li><a href="/register">Register</a></li>
			<li><a href="/login">Login</a></li>
		<?php else : ?>
			<li><a href="/profile">Account</a></li>
			<li><a href="/logout">Logout</a></li>
		<?php endif; ?>
	</ul>
</nav>