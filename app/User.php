<?php

namespace App;

use App\Core\Model;

class User extends Model{
	public $id;
	public $name;
	public $email;
	public $password;
	public $hash;
	public $activated;
}