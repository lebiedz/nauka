<?php
namespace App\Core\Database;
use PDO;
class QueryBuilder{
	private $pdo;
	public function __construct($pdo){
		$this->pdo = $pdo;
	}
	public function selectAll($table, $class){
		$statement = $this->pdo->prepare("select * from {$table}");
		$statement->execute();
		return $statement->fetchAll(PDO::FETCH_CLASS, $class);
	}
	public function selectObject($table, $parameters, $class){
		foreach (array_keys($parameters) as $param){
			$parameter[] = $param . "=:" . $param;
		}
		$sql = sprintf(
			"select * from %s where (%s)",
			$table,
			implode(' and ', $parameter)
		);
		$statement = $this->pdo->prepare($sql);
		$statement->execute($parameters);
		return $statement->fetchAll(PDO::FETCH_CLASS, $class);
	}
	public function select($table, $parameters){
		foreach (array_keys($parameters) as $param){
			$parameter[] = $param . "=:" . $param;
		}
		$sql = sprintf(
			"select * from %s where (%s)",
			$table,
			implode(' and ', $parameter)
		);
		$statement = $this->pdo->prepare($sql);
		$statement->execute($parameters);
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	public function insert($table, $parameters){
		$sql = sprintf(
			"insert into %s (%s) values (%s)",
			$table,
			implode(', ', array_keys($parameters)),
			':' . implode(', :', array_keys($parameters))
		);
		$statement = $this->pdo->prepare($sql);
		$statement->execute($parameters);
	}
	public function update_row($table, $column, $value, $parameters){
		foreach (array_keys($parameters) as $param){
			$parameter[] = $param . "=:" . $param;
		}
		$sql = sprintf(
			"update %s set %s=:value where (%s)",
			$table,
			$column,
			implode(' and ', $parameter)
		);
		$value = ['value' => $value];
		$parameters = array_merge($value, $parameters);
		$statement = $this->pdo->prepare($sql);
		$statement->execute($parameters);
	}
	public function delete($table, $parameters){
		foreach (array_keys($parameters) as $param){
			$parameter[] = $param . "=:" . $param;
		}
		$sql = sprintf(
			"delete from %s where (%s)",
			$table,
			implode(' and ', $parameter)
		);
		$statement = $this->pdo->prepare($sql);
		$statement->execute($parameters);
	}
}