<?php
use App\Core\Router;
use App\Core\Request;
require 'core/bootstrap.php';
Router::load('app/routes.php')
		->direct(Request::uri(), Request::method());
