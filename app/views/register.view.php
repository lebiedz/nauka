<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<?php include('partials/css.view.php'); ?>
</head>
<body>
	<?php include('partials/nav.view.php'); ?>
	<h3>Register:</h3>
	<?php if(isset($_SESSION['error'])) : ?>
		<div>
			<p><?= $_SESSION['error']; ?>
		</div>
	<?php endif; unset($_SESSION['error']); ?>
	<form action="/register" method="post">
		<input type="hidden" name="url" value="/register">
		<p>Name:</p>
		<input type="text" name="name" required>
		<p>E-Mail Address:</p>
		<input type="email" name="email" required>
		<p>Password:</p>
		<input type="password" name="password" required>
		<p>Confirm password:</p>
		<input type="password" name="password-confirmation" required>
		<br><br>
		<input type="submit" name="submit" value="Submit">
	</form>
</body>
</html>