<!DOCTYPE html>
<html>
<head>
	<title>Hello, <?= $user->name ?> &lt;<?= $user->email ?>&gt;</title>
	<?php include('partials/css.view.php'); ?>
</head>
<body>
	<h1>Hello, <?= $user->name ?> &lt;<?= $user->email ?>&gt;</h1>
	<?php include('partials/nav.view.php');
	if($user->activated === '0') : ?>
		<h3>Your account has not been activated yet. Please check your e-mail address provided in the registration form and click on the activation link.</h3>
	<?php else :
		if(isset($_SESSION['just_activated']) && $_SESSION['just_activated'] === 1) : ?>
			<p style="padding: 10px 0;">Your account has been successfully activated!</p>
		<?php endif; $_SESSION['just_activated'] = 0; ?>
		<h3>What would you like to do?</h3>
		<ul>
			<li><a href="/profile/change_password">Change password</a></li>
		</ul>
	<?php endif; ?>
</body>
</html>