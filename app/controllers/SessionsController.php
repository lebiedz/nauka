<?php
namespace App\Controllers;

use App\User;

class SessionsController extends Controller{
	public function __construct(){
		session_start();
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true){
			header('Location: /profile');
		}
	}
	public function create(){
		return views('login');
	}
	public function store(){
		if ($_POST['email'] === '' OR $_POST['password'] === ''){
			$_SESSION['error'] = "All fields are required";
			return header('Location: /login');
		}
		$user = User::select('users', ['email' => $_POST['email']]);
		if(count($user) === 0 OR !password_verify($_POST['password'], $user[0]['password'])){
			$_SESSION['error'] = "E-mail address and/or password are wrong.";
			return header('Location: /login');
		}
		if (password_verify($_POST['password'], $user[0]['password'])){
			$_SESSION['user_id'] = $user[0]['id'];
			$_SESSION['logged_in'] = true;
			header('Location: /profile');
		}
	}
	public function destroy(){
		session_destroy();
		header('Location: /login');
	}
}