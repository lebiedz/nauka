<?php

namespace App\Traits;

trait ValidatesRequests{
	public function validate_request($rules = []){
		if ($_SERVER['REQUEST_METHOD'] === 'POST'){
			$request = $_POST;
		} else {
			$request = $_GET;
		}
		foreach($request as $key => $value){
			if($value === ''){
				$_SESSION['error'] = "All fields are required.";
				header('Location: '. $request['url']);
			}
		}
		if (isset($request['password']) && $request['password'] !== $request['password-confirmation']){
			$_SESSION['error'] = "Input passwords don't match.";
			header('Location: '. $request['url']);
		}
		
	}
}