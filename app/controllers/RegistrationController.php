<?php
namespace App\Controllers;
use App\Core\App;
use App\User;
class RegistrationController extends Controller{
	public function __construct(){
		session_start();
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true){
			return header('Location: /profile');
		}
	}
	public function create(){
		return views('register');
	}
	public function store(){
		$this->validate_request();
		$users = User::select('users', ['email' => $_POST['email']]);
		if(count($users) > 0){
			$_SESSION['error'] = "The user already exists!";
			return header('Location: /register');	
		}
		$userdata = [
			'name' => $_POST['name'],
			'email' => $_POST['email'],
			'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
			'hash' => md5(rand())
		];
		User::insert('users', $userdata);
		$this->verification_mail($userdata['email'], $userdata['hash']);
		$_SESSION['user_id'] = User::select('users', ['email' => $userdata['email']])[0]['id'];
		$_SESSION['logged_in'] = true;
		header('Location: /profile');
	}
}