<!DOCTYPE html>
<html>
<head>
	<title>Reset your password</title>
	<?php include('partials/css.view.php'); ?>
</head>
<body>
	<?php include('partials/nav.view.php'); ?>
	<h3>Reset your password:</h3>
	<?php if(isset($_SESSION['error'])) : ?>
		<div>
			<p><?= $_SESSION['error']; ?>
		</div>
	<?php endif; unset($_SESSION['error']); ?>
	<form method="post" action="/profile/new_password">
		<input type="hidden" name="url" value="/profile/new_password">
		<p>New password:</p>
		<input type="password" name="new-pass">
		<p>Confirm new password:</p>
		<input type="password" name="new-pass-confirmation">
		<br><br>
		<input type="submit" name="submit" value="Submit">
	</form>
</body>
</html>