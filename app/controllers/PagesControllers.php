<?php
namespace App\Controllers;
use App\Core\App;
use App\Task;
class PagesControllers{
	public function __construct(){
		session_start();
	}
	public function home(){
		$tasks = Task::selectAll('tasks', 'App\\Task');

		return views('index', compact('tasks'));
	}
	public function about(){
		return views('about');
	}
	public function contact(){
		return views('contact');
	}
	public function form(){
		return views('form');
	}
}