<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<?php include('partials/css.view.php'); ?>
</head>
<body>
	<?php include('partials/nav.view.php'); ?>
	<h3>Login:</h3>
	<?php if(isset($_SESSION['error'])) : ?>
		<div>
			<p><?= $_SESSION['error']; ?>
		</div>
	<?php endif; unset($_SESSION['error']); ?>
	<form action="/login" method="post">
		<input type="hidden" name="url" value="/login">
		<p>E-Mail:</p>
		<input type="email" name="email" required>
		<p>Password:</p>
		<input type="password" name="password" required>
		<br><br>
		<input type="submit" name="submit" value="Submit">
	</form>
	<br>
	<a href='/profile/reset_password'>Forgot password?</a>
</body>
</html>