<!DOCTYPE html>
<html>
<head>
	<title>New Task Form</title>
	<?php include('partials/css.view.php'); ?>
</head>
<body>
	<?php include('partials/nav.view.php'); ?>
	<h3>Create a new task:</h3>
	<?php if(isset($_SESSION['error'])) : ?>
		<div>
			<p><?= $_SESSION['error']; ?>
		</div>
	<?php endif; unset($_SESSION['error']); ?>
	<form action="/new_task" method="post">
		<input type="hidden" name="url" value="/form">
		<p>Title:</p>
		<input type="text" name="title">
		<p>Description:</p>
		<input type="text" name="description">
		<br><br>
		<input type="submit" name="submit" value="Submit">
	</form>
</body>
</html>