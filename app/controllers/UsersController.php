<?php

namespace App\Controllers;

use App\User;

class UsersController extends Controller {
	private $user;
	public function __construct(){
		session_start();
		$this->user = User::selectObject('users', ['id' => $_SESSION['user_id']], '\\App\\User')[0];
	}
	public function index(){
		if (!(isset($_SESSION['logged_in']) OR $_SESSION['logged_in'] === true)){
			$_SESSION['error'] = 'Login first please!';
			header('Location: /login');
		}
		return views('profile', ['user' => $this->user]);
	}
	public function verify(){
		$hash = User::selectObject('users', ['hash' => $_GET['hash']], '\\App\\User')[0]->hash;
		$email = User::selectObject('users', ['email' => $_GET['email']], '\\App\\User')[0]->email;
		if(!array_key_exists('email', $_GET) OR !array_key_exists('hash', $_GET) OR !($hash && $email)){
			return header('Location: ');
		}
		User::update_row('users', 'activated', 1, ['email' => $email, 'hash' => $hash]);
		if(!isset($_SESSION) OR $_SESSION['logged_in'] !== true){
			$_SESSION['error'] = 'Your account has been activated. Login now to proceed';
			return header('Location: /login');
		}
		$_SESSION['just_activated'] = 1;
		return header('Location: /profile');
	}
}