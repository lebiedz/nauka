<?php
namespace App\Core;
class Router {
	public $routes = [
		'GET' => [],
		'POST' => []
	];
	private $singleton = null;
	protected $controller;
	public static function load($file){
		$router = new static;
		require $file;
		return $router;
	}
	public function define($routes){
		$this->routes = $routes;
	}
	public function get($uri, $controller){
		$this->routes['GET'][$uri] = $controller;
	}
	public function post($uri, $controller){
		$this->routes['POST'][$uri] = $controller;
	}
	public function direct($uri, $requestType){
			if (array_key_exists($uri, $this->routes[$requestType])){
				return $this->callAction(
					...explode('@', $this->routes[$requestType][$uri])
				);
			}
				throw new \Exception('Error');
	}
	protected function callAction($controller, $action){
		$controller = "App\\Controllers\\$controller";
		if(is_null($this->singleton)){
			$this->singleton = new $controller;
		} else{
			$this->singleton = $controller;
		}
		if (! method_exists($this->singleton, $action)){
			throw new \Exception("{$this->singleton} does not respond to {$action} action.");
		}
		return $this->singleton->$action();
	}
}