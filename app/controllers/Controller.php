<?php

namespace App\Controllers;

use App\Traits\ValidatesRequests;

class Controller {

	use ValidatesRequests;

	public function verification_mail($email, $hash){
		$link = 'http://' . $_SERVER['HTTP_HOST'] . '/profile/verify?email='.$email.'&hash='.$hash;
		$header = 'From: Admin <admin@projekty-treningowe.tk>';
		mail($email, 'Account verification', 'Hello,
			
Click on this link to activate your account:
'.$link, $header);
	}

}