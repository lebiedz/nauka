<?php
namespace App\Controllers;
use App\Task;
use App\Core\App;
class TasksController{
	public function newTask(){
		Task::insert('tasks', [
			'title' => $_POST['title'],
			'description' => $_POST['description']
		]);
		header('Location: ');
	}
	public function completeTask(){
		Task::update_row('tasks', 'completed', 1, ['id' => $_GET['id']]);
		header('Location: ');
	}
}