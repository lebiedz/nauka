<!DOCTYPE html>
<html>
<head>
	<title>Change your password</title>
	<?php include('partials/css.view.php'); ?>
</head>
<body>
	<?php include('partials/nav.view.php'); ?>
	<h3>Change your password:</h3>
	<?php if(isset($_SESSION['error'])) : ?>
		<div>
			<p><?= $_SESSION['error']; ?>
		</div>
	<?php endif; unset($_SESSION['error']); ?>
	<form method="post">
		<input type="hidden" name="url" value="/profile/change_pass">
		<p>Old Password:</p>
		<input type="password" name="oldpass" required>
		<p>New Password:</p>
		<input type="password" name="newpass" required>
		<p>Confirm New Password:</p>
		<input type="password" name="newpass-confirmation" required>
		<br><br>
		<input type="submit" name="submit" value="Submit">
	</form>
</body>
</html>