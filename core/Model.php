<?php

namespace App\Core;

use App\Core\Database\QueryBuilder;
use App\Core\Database\Connection;

class Model{

	private static $singleton = null;
	private $querybuilder;


	private function __construct(QueryBuilder $database = null){
		if(!is_null($database)){
			$this->querybuilder = $database;
		}
	}

	private function __clone(){}

	public static function create(QueryBuilder $database){
		if (is_null(static::$singleton)){
			return static::$singleton = new Model($database);
		}
		return static::$singleton;
	}

	private function newQuery(){
		if (is_null($this->querybuilder)){
			$this->querybuilder = new QueryBuilder(Connection::make(App::get('config')['database']));
		}
		return $this->querybuilder;
	}

	public static function selectAll($table, $class){
		return (new static)->newQuery()->selectAll($table, $class);
	}

	public function selectObject($table, $parameters, $class){
		return (new static)->newQuery()->selectObject($table, $parameters, $class);
	}

	public static function select($table, $parameters){
		return (new static)->newQuery()->select($table, $parameters);
	}

	public static function insert($table, $parameters){
		return (new static)->newQuery()->insert($table, $parameters);
	}

	public static function update_row($table, $column, $value, $parameters){
		return (new static)->newQuery()->update_row($table, $column, $value, $parameters);
	}

	public static function delete($table, $parameters){
		return (new static)->newQuery()->delete($table, $parameters);
	}
}