<?php
use App\Core\App;
use App\Core\Database\QueryBuilder;
use App\Core\Database\Connection;
use App\Core\Model;
require 'vendor/autoload.php';
require "app/functions.php";
App::bind('config', require_once 'config.php');
App::bind('database', new QueryBuilder(
	Connection::make(App::get('config')['database'])
));
App::bind('model', Model::create(App::get('database')));