<?php

namespace App\Controllers;

use App\User;

class PasswordController extends Controller{
	private $user;
	private $hash;
	private $email;

	public function __construct(){
		session_start();
		if(isset($_SESSION['user_id'])){
			$this->user = User::selectObject('users', ['id' => $_SESSION['user_id']], '\\App\\User')[0];
		}
	}
	public function change(){
		return views('change_pass');
	}
	public function store(){
		foreach($_POST as $value){
			if($value === ''){
				$_SESSION['error'] = "All fields are required.";
				header('Location: '. $_POST['url']);
			}
		}
		if (!password_verify($_POST['oldpass'], $this->user->password)){
			$_SESSION['error'] = 'Wrong old password';
			return header('Location: /profile/change_pass');
		}
		if ($_POST['newpass'] !== $_POST['newpass-confirmation']){
			$_SESSION['error'] = "New password doesn't match confirmation";
			return header('Location: /profile/change_pass');
		}
		User::update_row('users', 'password', password_hash($_POST['newpass'], PASSWORD_BCRYPT), ['id' => $_SESSION['user_id']]);
		return header('Location: /profile');
	}
	public function requestReset(){
		return views('reset_pass');
	}
	public function sendResetMail(){
		$hash = User::selectObject('users', ['email' => $_POST['reset-email']], '\\App\\User')[0]->hash;
		if(count($hash) === 0){
			$_SESSION['error'] = 'There is no account with such email.';
			return header('Location: /profile/reset_pass');
		}
		$link = 'http://' . $_SERVER['HTTP_HOST'] . '/profile/new_password?email=' . $_POST['reset-email'] . '&hash=' . $hash;
		$header = 'From: Admin <admin@projekty-treningowe.tk>';
		mail($_POST['reset-email'], 'Password reset', 'Hello,
			
We have received a request for password reset. To do so, click on the link below:
'.$link, $header);
		$_SESSION['error'] = 'Reset link sent to your e-mail. Click on it and follow further instructions to set new password.';
		return header('Location: /profile/reset_password');
	}
	public function setNewPassword(){
		$hash = User::selectObject('users', ['hash' => $_GET['hash']], '\\App\\User')[0]->hash;
		$email = User::selectObject('users', ['email' => $_GET['email']], '\\App\\User')[0]->email;
		if(!array_key_exists('email', $_GET) OR !array_key_exists('hash', $_GET) OR !($hash && $email)){
			return header('Location: /login');
		}
		$_SESSION['email'] = $email;
		$_SESSION['hash'] = $hash;
		return views('new_pass');
	}
	public function finalizeReset(){
		foreach($_POST as $key => $value){
			if($value === ''){
				$_SESSION['error'] = "All fields are required.";
				header('Location: '. $_POST['url']);
			}
		}
		User::update_row('users', 'password', password_hash($_POST['new-pass'], PASSWORD_BCRYPT), ['email' => $_SESSION['email'], 'hash' => $_SESSION['hash']]);
		$_SESSION['error'] = 'Your password has been updated. You can login now.';
		return header('Location: /login');
	}
}