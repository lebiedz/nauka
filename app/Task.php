<?php

namespace App;

use App\Core\Model;

class Task extends Model{
	public $id;
	public $title;
	public $description;
	public $completed;
}