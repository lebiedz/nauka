<!DOCTYPE html>
<html>
<head>
	<title>Homepage</title>
	<script>
		function newForm() {
   			window.open("/form","_self");
		}
	</script>
	<?php include('partials/css.view.php'); ?>
</head>
<body>
	<h1>Homepage</h1>
	<?php include('partials/nav.view.php'); ?>
	<h3>Your tasks for today:</h3>
	<ul>
		<?php foreach ($tasks as $task) : ?>
			<li name='<?= $task->id ?>'>
				<?php if ($task->completed) : ?>
					<strike><b><?= $task->title ?>:</b> <?= $task->description ?></strike> &#10004;
				<?php else : ?>
					<b><?= $task->title ?>:</b> <?= $task->description ?>
					<a href="/complete_task?id=<?= $task->id ?>" name="task_complete">Complete task</a>
				<?php endif ?>
			</li>
		<?php endforeach ?>
	</ul>
	<button name='newtask' type='button' onclick='newForm()'>Create a new task</button>
</body>
</html>